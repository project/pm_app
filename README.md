## Introduction

PM App is an experimental module provided along with Drupal PM to showcase
how a Decoupled Application could be build on top of Drupal PM to improve
user experience for daily use.

## Requirements

This module requires no modules is based on "Drupal PM" and its dependencies.

## Installation

*If you intend to contribute to Project Browser, skip this step and use the "Contributing" instructions instead*

Install with composer: `composer require drupal/pm` then enable the module `pm_app` along with its dependencies.


## Contributing

- In the `/pm/modules/pm_app` directory, install PHP dependencies with `composer install`
- In the `/pm/modules/pm_app/fem-knban` directory:
  - install JS dependencies with `pnpm install`
  - For development, run the dev script `pnpm run watch` which will watch for filesystem changes
    - Note 1: The application would be available in your Drupal site at `/pm-app`
    - Note 2: The watch tasks actually "builds" the application. So no extra build step is needed.


## Configuration

No configurations available as of now.

## Maintainers

- Shibin Das (D34dMan) - https://www.drupal.org/u/d34dman
