import type { Subtask } from "src/lib/board/task";

export default function filterUniqueStrings(subTasks: Subtask[]) {
  const seen = new Set();

  const res = [];
  subTasks.forEach((s) => {
    if (seen.has(s.id)) return;

    res.push(s);
    seen.add(s.id);
  });

  return res;
}