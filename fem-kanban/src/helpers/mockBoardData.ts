import type { BoardData } from "src/lib/board/board";

let id = 0;

function generateId() {
  return (id++).toString();
}

export const mockBoardData: BoardData[] = [
  {
    id: generateId(),
    name: "Platform Launch",
    boardType: "kanban",
    boardColumnType: "pm_epic",
    last_updated: 0,
    columns: [
      {
        id: generateId(),
        name: "Todo",
        boardColor: "#49C4E5",
        last_updated: 0,
        tasks: [
          {
            id: generateId(),
            title: "Build UI for onboarding flow",
            description: "",
            last_updated: 0,
            subtasks: [
              {
                id: generateId(),
                title: "Sign up page",
                isCompleted: true,
                last_updated: 0,
              },
              {
                id: generateId(),
                title: "Sign in page",
                isCompleted: false,
                last_updated: 0,
              },
              {
                id: generateId(),
                title: "Welcome page",
                isCompleted: false,
                last_updated: 0,
              },
            ],
          },
          {
            id: generateId(),
            title: "Build UI for search",
            description: "",
            last_updated: 0,
            subtasks: [
              {
                id: generateId(),
                title: "Search page",
                isCompleted: false,
                last_updated: 0,
              },
            ],
          },
          {
            id: generateId(),
            title: "Build settings UI",
            description: "",
            last_updated: 0,
            subtasks: [
              {
                id: generateId(),
                title: "Account page",
                isCompleted: false,
                last_updated: 0,
              },
              {
                id: generateId(),
                title: "Billing page",
                isCompleted: false,
                last_updated: 0,
              },
            ],
          },
          {
            id: generateId(),
            title: "QA and test all major user journeys",
            last_updated: 0,
            description:
              "Once we feel version one is ready, we need to rigorously test it both internally and externally to identify any major gaps.",
            subtasks: [
              {
                id: generateId(),
                title: "Internal testing",
                isCompleted: false,
                last_updated: 0,
              },
              {
                id: generateId(),
                title: "External testing",
                isCompleted: false,
                last_updated: 0,
              },
            ],
          },
        ],
      },
      {
        id: generateId(),
        name: "Doing",
        boardColor: "#8471F2",
        last_updated: 0,
        tasks: [
          {
            id: generateId(),
            title: "Design settings and search pages",
            description: "",
            last_updated: 0,
            subtasks: [
              {
                id: generateId(),
                title: "Settings - Account page",
                isCompleted: true,
                last_updated: 0,
              },
              {
                id: generateId(),
                title: "Settings - Billing page",
                isCompleted: true,
                last_updated: 0,
              },
              {
                id: generateId(),
                title: "Search page",
                isCompleted: false,
                last_updated: 0,
              },
            ],
          },
          {
            id: generateId(),
            title: "Add account management endpoints",
            description: "",
            last_updated: 0,
            subtasks: [
              {
                id: generateId(),
                title: "Upgrade plan",
                isCompleted: true,
                last_updated: 0,
              },
              {
                id: generateId(),
                title: "Cancel plan",
                isCompleted: true,
                last_updated: 0,
              },
              {
                id: generateId(),
                title: "Update payment method",
                isCompleted: false,
                last_updated: 0,
              },
            ],
          },
          {
            id: generateId(),
            title: "Design onboarding flow",
            description: "",
            last_updated: 0,
            subtasks: [
              {
                id: generateId(),
                title: "Sign up page",
                isCompleted: true,
                last_updated: 0,
              },
              {
                id: generateId(),
                title: "Sign in page",
                isCompleted: false,
                last_updated: 0,
              },
              {
                id: generateId(),
                title: "Welcome page",
                isCompleted: false,
                last_updated: 0,
              },
            ],
          },
          {
            id: generateId(),
            title: "Add search enpoints",
            description: "",
            last_updated: 0,
            subtasks: [
              {
                id: generateId(),
                title: "Add search endpoint",
                isCompleted: true,
                last_updated: 0,
              },
              {
                id: generateId(),
                title: "Define search filters",
                isCompleted: false,
                last_updated: 0,
              },
            ],
          },
          {
            id: generateId(),
            title: "Add authentication endpoints",
            description: "",
            last_updated: 0,
            subtasks: [
              {
                id: generateId(),
                title: "Define user model",
                isCompleted: true,
                last_updated: 0,
              },
              {
                id: generateId(),
                title: "Add auth endpoints",
                isCompleted: false,
                last_updated: 0,
              },
            ],
          },
          {
            id: generateId(),
            last_updated: 0,
            title:
              "Research pricing points of various competitors and trial different business models",
            description:
              "We know what we're planning to build for version one. Now we need to finalise the first pricing model we'll use. Keep iterating the subtasks until we have a coherent proposition.",
            subtasks: [
              {
                id: generateId(),
                title: "Research competitor pricing and business models",
                isCompleted: true,
                last_updated: 0,
              },
              {
                id: generateId(),
                title: "Outline a business model that works for our solution",
                isCompleted: false,
                last_updated: 0,
              },
              {
                id: generateId(),
                title:
                  "Talk to potential customers about our proposed solution and ask for fair price expectancy",
                isCompleted: false,
                last_updated: 0,
              },
            ],
          },
        ],
      },
      {
        id: generateId(),
        name: "Done",
        boardColor: "#67E2AE",
        last_updated: 0,
        tasks: [
          {
            id: generateId(),
            title: "Conduct 5 wireframe tests",
            last_updated: 0,
            description:
              "Ensure the layout continues to make sense and we have strong buy-in from potential users.",
            subtasks: [
              {
                id: generateId(),
                title: "Complete 5 wireframe prototype tests",
                isCompleted: true,
                last_updated: 0,
              },
            ],
          },
          {
            id: generateId(),
            title: "Create wireframe prototype",
            last_updated: 0,
            description:
              "Create a greyscale clickable wireframe prototype to test our asssumptions so far.",
            subtasks: [
              {
                id: generateId(),
                title: "Create clickable wireframe prototype in Balsamiq",
                isCompleted: true,
                last_updated: 0,
              },
            ],
          },
          {
            id: generateId(),
            last_updated: 0,
            title: "Review results of usability tests and iterate",
            description:
              "Keep iterating through the subtasks until we're clear on the core concepts for the app.",
            subtasks: [
              {
                id: generateId(),
                last_updated: 0,
                title:
                  "Meet to review notes from previous tests and plan changes",
                isCompleted: true,
              },
              {
                id: generateId(),
                title: "Make changes to paper prototypes",
                isCompleted: true,
                last_updated: 0,
              },
              {
                id: generateId(),
                title: "Conduct 5 usability tests",
                isCompleted: true,
                last_updated: 0,
              },
            ],
          },
          {
            id: generateId(),
            last_updated: 0,
            title:
              "Create paper prototypes and conduct 10 usability tests with potential customers",
            description: "",
            subtasks: [
              {
                id: generateId(),
                last_updated: 0,
                title: "Create paper prototypes for version one",
                isCompleted: true,
              },
              {
                id: generateId(),
                last_updated: 0,
                title: "Complete 10 usability tests",
                isCompleted: true,
              },
            ],
          },
          {
            id: generateId(),
            last_updated: 0,
            title: "Market discovery",
            description:
              "We need to define and refine our core product. Interviews will help us learn common pain points and help us define the strongest MVP.",
            subtasks: [
              {
                id: generateId(),
                last_updated: 0,
                title: "Interview 10 prospective customers",
                isCompleted: true,
              },
            ],
          },
          {
            id: generateId(),
            last_updated: 0,
            title: "Competitor analysis",
            description: "",
            subtasks: [
              {
                id: generateId(),
                last_updated: 0,
                title: "Find direct and indirect competitors",
                isCompleted: true,
              },
              {
                id: generateId(),
                last_updated: 0,
                title: "SWOT analysis for each competitor",
                isCompleted: true,
              },
            ],
          },
          {
            id: generateId(),
            last_updated: 0,
            title: "Research the market",
            description:
              "We need to get a solid overview of the market to ensure we have up-to-date estimates of market size and demand.",
            subtasks: [
              {
                id: generateId(),
                last_updated: 0,
                title: "Write up research analysis",
                isCompleted: true,
              },
              {
                id: generateId(),
                last_updated: 0,
                title: "Calculate TAM",
                isCompleted: true,
              },
            ],
          },
        ],
      },
    ],
  },
  {
    id: generateId(),
    last_updated: 0,
    name: "Marketing Plan",
    boardType: "kanban",
    boardColumnType: "pm_epic",
    columns: [
      {
        id: generateId(),
        last_updated: 0,
        name: "Todo",
        boardColor: "#49C4E5",
        tasks: [
          {
            id: generateId(),
            last_updated: 0,
            title: "Plan Product Hunt launch",
            description: "",
            subtasks: [
              {
                id: generateId(),
                last_updated: 0,
                title: "Find hunter",
                isCompleted: false,
              },
              {
                id: generateId(),
                last_updated: 0,
                title: "Gather assets",
                isCompleted: false,
              },
              {
                id: generateId(),
                last_updated: 0,
                title: "Draft product page",
                isCompleted: false,
              },
              {
                id: generateId(),
                last_updated: 0,
                title: "Notify customers",
                isCompleted: false,
              },
              {
                id: generateId(),
                last_updated: 0,
                title: "Notify network",
                isCompleted: false,
              },
              {
                id: generateId(),
                last_updated: 0,
                title: "Launch!",
                isCompleted: false,
              },
            ],
          },
          {
            id: generateId(),
            last_updated: 0,
            title: "Share on Show HN",
            description: "",
            subtasks: [
              {
                id: generateId(),
                last_updated: 0,
                title: "Draft out HN post",
                isCompleted: false,
              },
              {
                id: generateId(),
                last_updated: 0,
                title: "Get feedback and refine",
                isCompleted: false,
              },
              {
                id: generateId(),
                last_updated: 0,
                title: "Publish post",
                isCompleted: false,
              },
            ],
          },
          {
            id: generateId(),
            last_updated: 0,
            title: "Write launch article to publish on multiple channels",
            description: "",
            subtasks: [
              {
                id: generateId(),
                last_updated: 0,
                title: "Write article",
                isCompleted: false,
              },
              {
                id: generateId(),
                last_updated: 0,
                title: "Publish on LinkedIn",
                isCompleted: false,
              },
              {
                id: generateId(),
                last_updated: 0,
                title: "Publish on Inndie Hackers",
                isCompleted: false,
              },
              {
                id: generateId(),
                last_updated: 0,
                title: "Publish on Medium",
                isCompleted: false,
              },
            ],
          },
        ],
      },
      {
        id: generateId(),
        last_updated: 0,
        boardColor: "#df8fd9",
        name: "Doing",
        tasks: [],
      },
      {
        id: generateId(),
        last_updated: 0,
        boardColor: "#df8fd9",
        name: "Done",
        tasks: [],
      },
    ],
  },
  {
    id: generateId(),
    last_updated: 0,
    name: "Roadmap",
    boardType: "kanban",
    boardColumnType: "pm_epic",
    columns: [
      {
        id: generateId(),
        last_updated: 0,
        boardColor: "#df8fd9",
        name: "Now",
        tasks: [
          {
            id: generateId(),
            last_updated: 0,
            title: "Launch version one",
            description: "",
            subtasks: [
              {
                id: generateId(),
                last_updated: 0,
                title: "Launch privately to our waitlist",
                isCompleted: false,
              },
              {
                id: generateId(),
                last_updated: 0,
                title: "Launch publicly on PH, HN, etc.",
                isCompleted: false,
              },
            ],
          },
          {
            id: generateId(),
            last_updated: 0,
            title: "Review early feedback and plan next steps for roadmap",
            description:
              "Beyond the initial launch, we're keeping the initial roadmap completely empty. This meeting will help us plan out our next steps based on actual customer feedback.",
            subtasks: [
              {
                id: generateId(),
                last_updated: 0,
                title: "Interview 10 customers",
                isCompleted: false,
              },
              {
                id: generateId(),
                last_updated: 0,
                title: "Review common customer pain points and suggestions",
                isCompleted: false,
              },
              {
                id: generateId(),
                last_updated: 0,
                title: "Outline next steps for our roadmap",
                isCompleted: false,
              },
            ],
          },
        ],
      },
      {
        id: generateId(),
        last_updated: 0,
        boardColor: "#df8fd9",
        name: "Next",
        tasks: [],
      },
      {
        id: generateId(),
        last_updated: 0,
        boardColor: "#df8fd9",
        name: "Later",
        tasks: [],
      },
    ],
  },
];
