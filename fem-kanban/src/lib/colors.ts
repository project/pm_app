export type Color = {
    name: string;
    value: string;
};


export const indicatorPallet = {
    blue: "#60a5fa",
    gray: "#d1d5db",
    red:  "#ef4444",
    green: "#4ade80",
    yellow: "#facc15",
    indigo: "#818cf8",
    purple: "#c084fc",
    pink: "#ec4899",
};


export const indicatorPalletMap = new Map([
    ["blue", "#60a5fa"],
    ["gray", "#d1d5db"],
    ["red",  "#ef4444"],
    ["green", "#4ade80"],
    ["yellow", "#facc15"],
    ["indigo", "#818cf8"],
    ["purple", "#c084fc"],
    ["pink", "#ec4899"],
]);