import { Fifo } from 'toad-cache'

type CacheDataType = {
    etag: string;
    data: any;
}

export const projectCache = new Fifo<CacheDataType>(10, 0);
export const projectDetailCache = new Fifo<CacheDataType>(10, 0);

