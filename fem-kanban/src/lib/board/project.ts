import type { BoardData } from "./board";

export type Project = {
    id: string;
    title: string;
};

export type ProjectData = {
    id: string;
    name: string;
    boards: BoardData[];
};
