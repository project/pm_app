import { currentlyViewedProject } from "../stores/appState";
import { get } from 'svelte/store'
import { Drupal } from '@drupal-js-sdk/core';
import { DrupalAuth } from '@drupal-js-sdk/auth';
import { StorageInMemory } from '@drupal-js-sdk/storage';
import { AxiosClient, FetchClient } from '@drupal-js-sdk/xhr';
import axios from 'axios';
import type { Board } from "./board/board";
import { eventBus } from './eventBus';
import type { ProjectData } from "./board/project";
import { projectDetailCache } from "./cache";

// @TODO Get baseUrl from App state.
const baseUrl = "";

const config = {
  baseURL: `${baseUrl}`,
};
const sdk = new Drupal(config);
const client = new AxiosClient(axios);
sdk.setClientService(client);
sdk.setSessionService(new StorageInMemory());
const auth = new DrupalAuth(sdk);

export const drupalAuth = auth;


function getUrl(endpoint: string): string {
  // A Hack to detect activity.
  eventBus.dispatch("activityDetected");
  return `${baseUrl}/${endpoint}`;
}

export type Entity = { id: string; title: string; };

function getProjectIdFromContext() {
  const project = get(currentlyViewedProject);
  return project.id;
}

export async function fetchNewToken() {
  const token = await auth.getSessionToken().then(
    res => res.data
  );
  return token;
}

async function getToken() {
  const token = await fetchNewToken();
  return `${token}`;
}
/**
 * Project State.
 */
export async function loadProjectsFromDrupal() {
  try {
    const response = await fetch(getUrl("api/pm_app/project"), {
      headers: {
        "Content-Type": "application/json"
      },
    });
    const result = await response.json();
    return result;
  } catch (error) {
    return [];
  }
}

/**
 * Project State.
 */
export async function loadBoardsFromDrupal() {
  try {
    const url = getUrl("api/pm_app/project/" + getProjectIdFromContext());
    const cacheItem = projectDetailCache.get(url);
    let response = undefined;
    if (cacheItem !== undefined) {
      response = await fetch(url, {
        headers: {
          "Content-Type": "application/json",
          "If-None-Match": cacheItem.etag,
        },
      });
      if (response.status == 304) {
        return cacheItem.data;
      }
    }
    else {
      response = await fetch(url, {
        headers: {
          "Content-Type": "application/json",
        },
      });
    }
    let newEtag = undefined;
    response.headers.forEach((val, key) => {
      if (key == 'x-drupal-etag') {
        newEtag = val;
      }
    });
    const result = await response.json();
    projectDetailCache.set(url, {etag: newEtag, data: result});
    return result;
  } catch (error) {
    return [];
  }
}

export async function saveBoardsToDrupal(boards) {
  try {
    const token = await fetchNewToken();
    const response = await fetch(getUrl("api/pm_app/project/" + getProjectIdFromContext()), {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        "X-CSRF-Token": token,
      },
      body: JSON.stringify({ id: getProjectIdFromContext(), boards }),
    });
    const result = await response.json();
    eventBus.dispatch('serverBoardDataRecieve', result);
  } catch (error) {
  }
}

export async function getParsedBoardsFromDrupal() {
  const response = await loadBoardsFromDrupal();
  return response.boards;
}

export async function deleteBoardFromDrupal(board: Board) {
  try {
    const token = await fetchNewToken();
    const response = await fetch(getUrl("pm/board/" + board.id), {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        "X-CSRF-Token": token,
      },
    });
    const result = await response.json();
    return true;
  } catch (error) {
    return false;
  }
}