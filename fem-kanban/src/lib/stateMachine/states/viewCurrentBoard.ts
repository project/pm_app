import { updateCurrentBoard } from "../../../stores/boardData";
import { eventBus } from "../../../lib/eventBus";
import { stateMachine } from "../stateMachine";
import type { StateMachineState } from "../types";
import { isIdling, lastActivityTime } from "../../../stores/appState";
import { get } from "svelte/store";

export const ViewCurrentBoardState: StateMachineState = {
  name: "viewCurrentBoard",

  onEnter() {
    updateCurrentBoard();
    this.onDoUserLogoutUnsub = eventBus.subscribe("doUserLogout", () => {
      stateMachine.transition("logout");
    });

    this.onViewTaskUnsub = eventBus.subscribe("viewTask", (task) => {
      stateMachine.transition("viewTask", task);
    });

    this.addTaskUnsub = eventBus.subscribe("addNewTask", () => {
      stateMachine.transition("addNewTask");
    });

    this.editBoardUnsub = eventBus.subscribe("editBoard", (board) => {
      stateMachine.transition("editBoard", board);
    });

    this.deleteBoardUnsub = eventBus.subscribe("deleteBoard", (board) => {
      stateMachine.transition("deleteBoard", board);
    });

    this.addBoardUnsub = eventBus.subscribe("addNewBoard", (board) => {
      stateMachine.transition("addNewBoard", board);
    });

    this.selectProjectUnsub = eventBus.subscribe("selectProject", () => {
      stateMachine.transition("selectProject");
    });

    // Set idling only when we are in this state and some time has passed since user did something.
    this.timeoutOutIdForIdleTimer = setInterval(() => {
      const idleTimeInSeconds = (Date.now() - get(lastActivityTime)) / 1000;
      isIdling.set(idleTimeInSeconds > 3);
      if (idleTimeInSeconds > 5) {
        // stateMachine.transition("loading");
        eventBus.dispatch("fetchDataFromServer");
      }
    }, 1000)
  },

  onExit() {
    isIdling.set(false);
    clearInterval(this.timeoutOutIdForIdleTimer);
    this.onDoUserLogoutUnsub();
    this.selectProjectUnsub();
    this.onViewTaskUnsub();
    this.addTaskUnsub();
    this.editBoardUnsub();
    this.deleteBoardUnsub();
    this.addBoardUnsub();
  },
};
