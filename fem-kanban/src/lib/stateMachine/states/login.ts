import { eventBus } from "../../eventBus";
import type { StateMachineState } from "../types";
import { stateMachine } from "../stateMachine";
import { currentState, currentlyViewedProject } from "../../../stores/appState";
import { drupalAuth, loadProjectsFromDrupal } from "../../drupal";
import { loadProjects } from "../../../stores/project";
import {projects} from "../../../stores/project";
import { get } from "svelte/store";

export const LoginState: StateMachineState = {
  name: "login",

  async onEnter() {
    this.loginUnsub = eventBus.subscribe("loginSubmit", (loginData) => {
      drupalAuth
        .login(loginData.name, loginData.password)
        .then(data => {
          stateMachine.transition("init");
        })
        .catch((error) => {
          console.log("Login Failed");
      })
    });
    this.loginCancelUnsub = eventBus.subscribe("loginCancel", () => {
      // @TODO: We don't have a scenario where the app is usefull without a project
      // in context.
    });
  },

  onExit() {
    this.loginUnsub();
    this.loginCancelUnsub();
  },

};
