import { eventBus } from "../../../lib/eventBus";
import type { StateMachineState } from "../types";
import { stateMachine } from "../stateMachine";
import { currentlyViewedProject } from "../../../stores/appState";
import { loadProjectsFromDrupal } from "../../../lib/drupal";
import { loadProjects } from "../../../stores/project";
import {projects} from "../../../stores/project";
import { get } from "svelte/store";

export const InitState: StateMachineState = {
  name: "init",

  async onEnter() {
    await this.loadProjects();
    const projectList = get(projects);
    if (projectList.length) {
        currentlyViewedProject.set(projectList[0]);
        stateMachine.transition("loading");
    }
    else {
        // @TODO Check if user is logged in or not.
        stateMachine.transition("selectProject");
    }    
  },

  async loadProjects() {
    const drupalData = await loadProjectsFromDrupal();
    loadProjects(drupalData);
  },
};
