import { eventBus } from "../../../lib/eventBus";
import type { StateMachineState } from "../types";
import { stateMachine } from "../stateMachine";
import { currentlyViewedProject } from "../../../stores/appState";
import { loadProjectsFromDrupal } from "../../../lib/drupal";
import { loadProjects } from "../../../stores/project";
import {projects} from "../../../stores/project";
import { get } from "svelte/store";
import { loadAppStateFromLocalStorage } from "../../../lib/saver";
import { colorScheme, sidebarExpanded } from "../../../stores/appControls";

export const InitAppState: StateMachineState = {
  name: "initApp",

  async onEnter() {
    this.loadAppState();
    this.initAppModalCancelUnSub = eventBus.subscribe("initAppModalCancel", () => {
      // @TODO: We don't have a scenario where the app is usefull without a project
      // in context.
      stateMachine.transition("resetApp");
    });
  },

  loadAppState() {
    const appState = loadAppStateFromLocalStorage();
    // @TODO: Save current board in localstorage as well.
    if (appState === null) return;

    colorScheme.set(appState.colorScheme);
    sidebarExpanded.set(appState.sidebarExpanded);
  }
};
