import { eventBus } from "../../../lib/eventBus";
import type { StateMachineState } from "../types";
import { stateMachine } from "../stateMachine";
import { currentlyViewedProject } from "../../../stores/appState";
import { loadProjectsFromDrupal } from "../../../lib/drupal";
import { loadProjects } from "../../../stores/project";

export const SelectProjectState: StateMachineState = {
  name: "selectProject",

  onEnter() {
    this.projectSelectedUnsub = eventBus.subscribe("projectSelected", (project) => {
      currentlyViewedProject.set(project);
      stateMachine.transition("loading");
    });
    this.projectCloseUnsub = eventBus.subscribe("selectProjectClose", () => {
      // @TODO: We don't have a scenario where the app is usefull without a project
      // in context.
    });
  },

  onExit() {
    this.projectSelectedUnsub();
    this.projectCloseUnsub();
  },
};
