import { boardToDelete } from "../../../stores/appState";
import { eventBus } from "../../../lib/eventBus";
import type { StateMachineState } from "../types";
import { stateMachine } from "../stateMachine";
import { deleteBoard } from "../../../stores/boardData";
import { deleteBoardFromDrupal } from "../../../lib/drupal";

export const DeleteBoardState: StateMachineState = {
  name: "deleteBoard",

  onEnter(board) {
    this.currentBoard = board;
    boardToDelete.set(board);

    this.cancelUnsub = eventBus.subscribe("deleteBoardCancel", () => {
      stateMachine.transition("viewCurrentBoard");
    });

    this.confirmUnsub = eventBus.subscribe("deleteBoardConfirm", () => {
      // @TODO delete board in Drupal and then remove from APP.
      deleteBoardFromDrupal(board).then((result) => {
        debugger
        deleteBoard(board);
        stateMachine.transition("viewCurrentBoard");
      })
      .catch((foo) => {
        debugger;
      });
      
    });
  },

  onExit() {
    delete this.currentBoard;
    boardToDelete.set(undefined);

    this.cancelUnsub();
    this.confirmUnsub();
  },
};
