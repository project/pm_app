import { eventBus } from "../../eventBus";
import type { StateMachineState } from "../types";
import { stateMachine } from "../stateMachine";
import { cleanupAppState, currentlyViewedProject } from "../../../stores/appState";
import { drupalAuth, loadProjectsFromDrupal } from "../../drupal";
import { cleanupProjects } from "../../../stores/project";
import { cleanupBoards } from "../../../stores/boardData";

export const LogoutState: StateMachineState = {
  name: "logout",

  async onEnter() {
    await drupalAuth.forcedLogout();
    const loginStatus = await drupalAuth.loginStatus();
    if (!loginStatus) {
      cleanupBoards();
      cleanupProjects();
      cleanupAppState();
      stateMachine.transition("resetApp");
    }
  },


};
