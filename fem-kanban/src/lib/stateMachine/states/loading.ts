import {
  loadAppStateFromLocalStorage,
  loadBoardsFromLocalStorage,
} from "../../../lib/saver";
import { Board } from "../../../lib/board/board";
import { boards, cleanupBoards, currentBoardId, loadBoards, setCurrentBoardId} from "../../../stores/boardData";
import { mockBoardData } from "../../../helpers/mockBoardData";
import { stateMachine } from "../stateMachine";
import type { StateMachineState } from "../types";
import { colorScheme, sidebarExpanded } from "../../../stores/appControls";
import {getParsedBoardsFromDrupal} from "../../drupal";
import { eventBus } from "../../../lib/eventBus";
import { get } from "svelte/store";
import { lastSyncWithServer } from "../../../stores/appState";

export const LoadingState: StateMachineState = {
  name: "loading",

  async onEnter() {
    await this.loadBoards();
    stateMachine.transition("viewCurrentBoard");
  },

  async loadBoards() {
    const drupalData = await getParsedBoardsFromDrupal();
    if (drupalData != undefined) {
      cleanupBoards();
      loadBoards(drupalData.map((bd) => Board.loadFromData(bd)));
      lastSyncWithServer.set(new Date().getTime());
      // @TODO Refactor the logic to check if id is in received data and switch accordingly.
      const currentId = get(currentBoardId);
      if ( currentId == undefined || !!currentId) {
        if (drupalData[0] !== undefined && drupalData[0]['id'] !== undefined) {
          setCurrentBoardId(`${drupalData[0]['id']}`);
        }
      } 
      
    }
  }
};
