import { eventBus } from "../../../lib/eventBus";
import type { StateMachineState } from "../types";
import { stateMachine } from "../stateMachine";
import { currentlyViewedProject } from "../../../stores/appState";
import { drupalAuth, loadProjectsFromDrupal } from "../../../lib/drupal";
import { loadProjects } from "../../../stores/project";
import {projects} from "../../../stores/project";
import { get } from "svelte/store";
import { boards } from "../../../stores/boardData";

export const ResetAppState: StateMachineState = {
  name: "resetApp",
  async onEnter() {
    projects.set([]);
    boards.set([]);
    const loginStatus = await drupalAuth.loginStatus();
    if (loginStatus) {
      stateMachine.transition("init");
    }
    else {
      stateMachine.transition("login");
    }
  },
};
