import type { InputListAction } from "src/components/Interactive/InputList/InputList.svelte";
import type { Board } from "src/lib/board/board";
import type { Entity } from "src/lib/drupal";
import type { TaskData, Task, Subtask } from "src/lib/board/task";
import type { Project, ProjectData } from "src/lib/board/project";

/**
 * Mapping of event names to payloads
 */
export type EventTypes = {
  // App State Events
  colorSchemeToggled: 'dark' | 'light';
  sidebarToggled: boolean;
  activityDetected: void;

  // Data Events
  taskUpdated: Task;
  boardUpdated: Board;

  // ProjecT Event
  selectProject: void;
  selectProjectClose: void;
  projectSelected: Project;

  // State Machine Events
  stateTransition: string;

  // View Task State Events
  viewTask: Task;
  viewTaskClose: void;
  viewTaskToggleSubtask: string;
  viewTaskUpdateStatus: string;

  // Edit Task State Events
  editTask: void;
  editTaskCancel: void;
  editTaskUpdate: TaskData;

  // Delete Task Events
  deleteTask: Task;
  deleteTaskCancel: void;
  deleteTaskConfirm: void;

  // Add Task State Events
  addNewTask: void;
  addNewTaskCreate: {
    title: string;
    description: string;
    subtasks: Subtask[];
    status: string;
  };
  addNewTaskCancel: void;

  // Edit Board Events
  editBoard: Board;
  editBoardCancel: void;
  editBoardUpdate: {
    name: string;
    columns: Entity[];
    columnActions: InputListAction;
  };

  // Delete Board Events
  deleteBoard: Board;
  deleteBoardCancel: void;
  deleteBoardConfirm: void;

  // Add Board Events
  addNewBoard: void;
  addNewBoardCancel: void;
  addNewBoardCreate: {
    name: string;
    columns: Entity[];
    boardType: string;
    boardColumnType: string;
  };

  // Login Events.
  loginSubmit: {
    name: string;
    password: string;
  };
  loginCancel: void;
  doUserLogout: void;

  initAppModalCancel: void;

  // server Tasks
  fetchDataFromServer: void;
  sendDataToServer: void;
  serverBoardDataRecieve: ProjectData;
};

export type Events = keyof EventTypes;

export type EventCallback<E extends Events> = (data?: EventTypes[E]) => void;
