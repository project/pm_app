
import type { Project } from "../lib/board/project";
import { derived, get, writable } from "svelte/store";

export const projects = writable([] as Project[]);

export function loadProjects(projectData: Project[]) {
    projects.set(projectData);
}

export function cleanupProjects() {
    projects.set([]);
}