/** @type {import('tailwindcss').Config} */
const themeOverrides = {
	neutral: '#111111',
	error: '#DD0000',
	'error-content': '#FFFFFF',
	warning: '#FFA500',
	'--rounded-box': '0.125rem', // border radius rounded-box utility class, used in card and other large boxes
	'--rounded-btn': '0.125rem', // border radius rounded-btn utility class, used in buttons and similar element
	'--rounded-badge': '0.125rem', // border radius rounded-badge utility class, used in badges and similar
	'--animation-btn': '0.125s', // duration of animation when you click on button
	'--animation-input': '0.2s', // duration of animation for inputs like checkbox, toggle, radio, etc
	'--btn-focus-scale': '0.95', // scale transform of button when you focus on it
	'--border-btn': '1px', // border width of buttons
	'--tab-border': '1px', // border width of tabs
	'--tab-radius': '0.125rem' // border radius of tabs
};
export default {
	content: ['./index.html', './src/**/*.{svelte,js,ts,jsx,tsx}'],
	theme: {
		extend: {}
	},
	daisyui: {
		darkTheme: 'night',
		themes: [
			{
				light: {
					...require('daisyui/src/theming/themes')['winter'],
					...themeOverrides
				}
			},
			{
				dark: {
					...require('daisyui/src/theming/themes')['night'],
					...themeOverrides
				}
			}
		]
	},
	plugins: [require('daisyui')]
};
