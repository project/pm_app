<?php

declare(strict_types=1);

namespace Drupal\pm_app\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Utility to interact with configurations for PM App.
 */
final class PmAppConfig {

  /**
   * Constructs a PmAppConfig object.
   */
  public function __construct(
    private readonly ConfigFactoryInterface $configFactory,
    private readonly EntityTypeManagerInterface $entityTypeManager,
  ) {}

  /**
   * Get configuration for Kanban Boards.
   */
  public function getKanbanBoardConfiguration(): array {
    return [
      "defaults" => $this->getDefaultValuesForEntityCreation(),
    ];
  }

  /**
   * Get default value for quickly adding a PM entity.
   *
   * @return array[]
   *   The default value for creating entities.
   */
  protected function getDefaultValuesForEntityCreation(): array {
    // @TODO: Convert to DTO for better DX.
    // @TODO: Evaluate if this should be explicitly provided during REST calls.
    return [
      "create" => [
        "bundle" => [
          "pm_epic" => "epic",
          "pm_feature" => "feature",
          "pm_story" => "story",
          "pm_task" => "task",
          "pm_sub_task" => "sub_task",
          "pm_board" => "kanban",
        ],
      ],
    ];
  }

}
