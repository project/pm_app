<?php

declare(strict_types=1);

namespace Drupal\pm_app\Service;

use Drupal\Core\Database\IntegrityConstraintViolationException;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\pm\PmContentEntityBaseInterface;
use Drupal\pm\Service\PmConfig;
use Drupal\pm\Service\PmHierarchy;
use Drupal\pm_board\Entity\PmBoardColumn;
use Drupal\pm_board\PmBoardInterface;
use Drupal\pm_project\PmProjectInterface;

/**
 * The Kanban Board Adapter.
 *
 * Adapts the PM App's api to "Drupal PM" entity and vice versa.
 */
final class PmAppKanbanService {

  /**
   * The Project Entity.
   *
   * @var \Drupal\pm_project\PmProjectInterface
   */
  private PmProjectInterface $project;

  /**
   * The key-value storage.
   */
  private readonly KeyValueStoreInterface $storage;

  /**
   * Constructs a PmAppKanbanService object.
   */
  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly LoggerChannelInterface $logger,
    private readonly KeyValueFactoryInterface $keyValueFactory,
    private readonly PmAppConfig $config,
    private readonly PmHierarchy $hierarchy,
    private readonly PmConfig $pmConfig,
  ) {
    $this->storage = $this->keyValueFactory->get('pm_app_rest_api_entity_map');
  }


  /**
   * Check if data is available.
   */
  public function has($project_id) {
    return (bool) $this
      ->entityTypeManager
      ->getStorage('pm_project')
      ->load($project_id);
  }

  /**
   * Get boards.
   */
  public function get($project_id, $default = NULL) {
    $pm_project = $this
      ->entityTypeManager
      ->getStorage('pm_project')
      ->load($project_id);
    return $this->loadAllBoards($pm_project);
  }

  /**
   * Set Project.
   */
  protected function setProject(PmProjectInterface $project) {
    $this->project = $project;
  }

  /**
   * Get Current project in context.
   */
  protected function getProject(): PmProjectInterface {
    return $this->project;
  }

  /**
   * Save data.
   */
  public function set($project_id, $response) {
    $project = $this->entityTypeManager->getStorage('pm_project')->load($project_id);
    $this->setProject($project);
    $this->processAllBoards($response['boards']);
  }

  protected function checkIfListItemsIsDirty($current_items, $new_items): bool {
    $is_dirty = false;
    if (count($current_items) == count($new_items)) {
      if (!empty($current_items)) {
        foreach ($current_items as $key => $val) {
          // Case where new item is present.
          if (empty($val[$key]['id'])) {
            $is_dirty = true;
            break;
          }
          // Case where order of the item was adjusted.
          if ($new_items[$key]['id'] != $val[$key]['id']) {
            $is_dirty = true;
            break;
          }
        }
      }
    }
    else {
      $is_dirty = true;
    }
    return $is_dirty;
  }

  /**
   * Check if value is different.
   */
  protected function checkIsDirty(string $parent_key, mixed $current_val, mixed $new_val): bool {
    $scope = $this->getBoardDataUpdateScope();
    $is_dirty = false;
    if(isset($scope[$parent_key])) {
      $current_scope = $scope[$parent_key];
      foreach ($current_scope as $key_to_check) {
        $sub_parent_key = $parent_key . '.' . $key_to_check;
        if (isset($scope[$sub_parent_key])) {
          if (is_array($new_val[$key_to_check])) {
            $is_dirty = $this->checkIfListItemsIsDirty($current_val[$key_to_check], $new_val[$key_to_check]);
            if ($is_dirty) break;
            foreach($new_val[$key_to_check] as $new_sub_key => $new_sub_val) {
              $is_dirty = $this->checkIsDirty($sub_parent_key, $current_val[$key_to_check][$new_sub_key], $new_sub_val);
              if ($is_dirty) break;
            }
          }
          else {
            $is_dirty = $this->checkIsDirty($sub_parent_key, $current_val[$key_to_check], $new_val[$key_to_check]);
          }
        }
        elseif (!isset($current_val[$key_to_check])) {
          $is_dirty = true;
        }
        elseif($new_val[$key_to_check] != $current_val[$key_to_check]) {
          $is_dirty = true;
        }
        if ($is_dirty) break;
      }
    }
    else {
      $is_dirty = $new_val != $current_val;
    }
    return $is_dirty;
  }

  /**
   * Get configuration option for generating content.
   *
   * @return array[]
   *   The Configuration.
   */
  protected function getConfig(): array {
    return $this->config->getKanbanBoardConfiguration();
  }

  /**
   * Load all kanban boards applicable for a given project.
   */
  protected function loadAllBoards(PmProjectInterface $project): array {
    $this->setProject($project);
    $boardStorage = $this->entityTypeManager->getStorage('pm_board');
    // 1. Load all boards belonging to the project.
    // 1.a. For each board find all Columns.
    // 1.a.1 For each column, find all Tasks.
    // 1.a.1.a For each Tasks find all Sub-Tasks.
    // Do reverse lookup to find all boards.
    $query = $boardStorage
      ->getQuery();
    $result = $query
      ->condition('pm_project', $project->id())
      ->accessCheck(TRUE)
      ->execute();
    $boards = $boardStorage->loadMultiple($result);
    $output = [];
    /** @var \Drupal\pm_board\PmBoardInterface $board */
    foreach ($boards as $board) {
      $output[] = $this->getBoardData($board);
    }
    return [
      "id" => $project->id(),
      "name" => $project->label(),
      "key" => $project->get('project_key')->value,
      "time" => date('Ymd H:i:s'),
      "boards" => $output,
    ];
  }

  /**
   * Get Board.
   */
  protected function getBoardData(PmBoardInterface $board): array {
    $columns = $this->getChildren($board);
    $columnData = [];
    foreach ($columns as $column) {
      $columnData[] = $this->getColumnData($column);
    }
    return [
      "id" => $board->id(),
      "name" => $board->label(),
      "type" => $board->bundle(),
      "column_type" => $board->get('pm_board_column_type')->entity->id(),
      "columns" => $columnData,
      "last_updated" => $board->getChangedTime(),
    ];
  }

  /**
   * Get child entities based on hierarchy.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity for which the child item needs to be found.
   *
   * @return array
   *   The child entities.
   */
  protected function getChildren(EntityInterface $entity): array {
    return $this->hierarchy->getChildren($entity);
  }

  /**
   * Generate scope for patch generator.
   *
   * @return array[]
   */
  protected function getBoardDataUpdateScope() {
    return [
      '' => [
        'id',
        'name',
        'boards',
      ],
      'boards' => [
        'id',
        'name',
//        'type',
//        'column_type',
        'columns',
      ],
      'boards.columns' => [
        'id',
        'name',
        'boardColor',
        'tasks',
      ],
      'boards.columns.tasks' => [
        'id',
        'title',
        'description',
        'subtasks',
      ],
      'boards.columns.tasks.subtasks' => [
        'id',
        'title',
        'isCompleted',
      ],
    ];
  }

  /**
   * Get Column.
   */
  protected function getColumnData(PmBoardColumn $column): array {
    $tasks = [];
    foreach ($this->getChildren($column) as $task) {
      $tasks[] = $this->getTaskData($task);
    }
    $color = $column->get('color')->getValue();
    return [
      "id" => $column->id(),
      "name" => $column->label(),
      "boardColor" => ($color) ? $color[0]['value'] : 'gray',
      "tasks" => $tasks,
      "last_updated" => $column->getChangedTime(),
    ];
  }

  /**
   * Get Task.
   */
  protected function getTaskData(PmContentEntityBaseInterface $task): array {
    $subtasks = [];
    foreach ($this->getChildren($task) as $subtask) {
      $subtasks[] = $this->getSubtaskData($subtask);
    }
    $output = [
      "id" => $task->id(),
      "key" => $task->get('pm_key')->value,
      "title" => $task->label(),
      "description" => ($description = $task->get('summary')->getValue()) ? $description[0]['value'] : '',
      "subtasks" => $subtasks,
      "last_updated" => $task->getChangedTime(),
    ];
    return $output;
  }

  /**
   * Get Subtask.
   */
  protected function getSubtaskData(EntityInterface $subtask): array {
    $output = [
      "id" => $subtask->id(),
      "key" => $subtask->get('pm_key')->value,
      "title" => $subtask->label(),
      "isCompleted" => $subtask->isDone(),
      "last_updated" => $subtask->getChangedTime(),
    ];
    return $output;
  }

  /**
   * Process Boards.
   */
  protected function processAllBoards($data) {
    $boardStorage = $this->entityTypeManager->getStorage('pm_board');
    foreach ($data as $board_data) {
      $external_id = $board_data['id'];
      $drupal_id = $this->getDrupalId($external_id);
      if ($drupal_id) {
        $board = $boardStorage->load($drupal_id);
        $baord_type = $board->bundle();
        // Bail out early in case of conflict.
        if (!empty($board_data['last_updated']) && $board->getChangedTime() != $board_data['last_updated']) {
          throw new IntegrityConstraintViolationException('Board data is outdated');
          return $board;
        }
        // @todo handle column that has been deleted.
        $current_data = $this->getBoardData($board);
        // Bail out early if things are same.
        if (!$this->checkIsDirty('boards', $current_data, $board_data)) {
          continue;
        }
      }
      else {
        $baord_type = $board_data['boardType'];
        $board = $boardStorage->create(
          [
            'bundle' => $baord_type,
            'pm_project' => $this->getProject()->id(),
            'pm_board_column_type' => $board_data['boardColumnType'],
          ]
        );
        $board->save();
        $drupal_id = $board->id();
        $this->setDrupalId($external_id, $drupal_id);
      }
      $board->set('label', $board_data['name']);
      $columns = [];
      foreach ($board_data['columns'] as $column_data) {
        $column_data['bundle'] = $board->pm_board_column_type->entity->id();
        $columns[] = $this->processColumn($column_data, $board);
      }
      $board_children_ref_field = $this->pmConfig->getChildReferenceFieldName('pm_board', $baord_type);
      $board->set($board_children_ref_field, $columns);
      if ($board->validate()->count() > 0) {
        throw new IntegrityConstraintViolationException('Board data is corrupted');
      }
      $board->save();
    }
  }

  /**
   * Process Column.
   */
  protected function processColumn($column_data, $board): PmBoardColumn {
    $column_type = $column_data['bundle'];
    $column_children_type = $this->pmConfig->getChildEntityType('pm_board_column', $column_type);
    $column_children_ref_field = $this->pmConfig->getChildReferenceFieldName('pm_board_column', $column_type);
    $storage = $this
      ->entityTypeManager
      ->getStorage('pm_board_column');
    $external_id = $column_data['id'];
    $drupal_id = $this->getDrupalId($external_id);
    /** @var EntityInterface $column */
    if ($drupal_id) {
      $column = $storage->load($drupal_id);
      $current_data = $this->getColumnData($column);
      // Bail out early if things are same.
      if (!$this->checkIsDirty('boards.columns', $current_data, $column_data)) {
        return $column;
      }
    }
    else {
      $column = $storage->create(
      [
        'bundle' => $column_type,
      ]
      );
      $column->save();
      $drupal_id = $column->id();
      $this->setDrupalId($external_id, $drupal_id);
    }
    $column->set('label', $column_data['name']);
    $column->set('color', $column_data['boardColor']);
    $column->set('color', 'gray');
    $items = [];
    foreach ($column_data['tasks'] as $item_data) {
      $item_data['entity_type'] = $column_children_type;
      $items[] = $this->processTask($item_data);
    }
    $column->set($column_children_ref_field, $items);
    // @TODO: if an item is getting added to a column, then it will be
    // removed from another column.
    $this->adjustItemsInOtherColumnsIfNeeded($items, $column, $board);

    $validation = $column->validate();
    // The following might have to be turned off when board data is valid.
    if ($validation->count() > 0) {
       throw new IntegrityConstraintViolationException('Column has corrupted data');
    }
    $column->save();
    return $column;
  }

  function adjustItemsInOtherColumnsIfNeeded($items, $column, $board) {
    $column_storage = $this->entityTypeManager->getStorage('pm_board_column');
    $board_children_ref_field = $this->pmConfig->getChildReferenceFieldName('pm_board', $board->bundle());
    $column_children_ref_field = $this->pmConfig->getChildReferenceFieldName('pm_board_column', $column->bundle());
    $board_columns = $board->get($board_children_ref_field)->referencedEntities();
    $other_board_columns = [];
    foreach ($board_columns as $board_column) {
      if ($board_column->id() != $column->id()) {
        $other_board_columns[$board_column->id()] = $board_column;
      }
    }
    if (empty($other_board_columns)) {
      return ;
    }
    /** @var EntityInterface $item */
    foreach ($items as $item) {
      $results = $column_storage
        ->getQuery()
        ->accessCheck(FALSE)
        ->condition($column_children_ref_field, $item->id())
        ->condition('id', array_keys($other_board_columns), 'IN')
        ->execute();
      foreach ($results as $result) {
        $other_board_column = $other_board_columns[$result];
        // Remove $item from $other_board_column.
        $other_board_column_items = $other_board_column->get($column_children_ref_field)->getValue();
        $adjusted_items = [];
        foreach ($other_board_column_items as $other_board_column_value) {
          if ($other_board_column_value['target_id'] == $item->id()) {
            continue;
          }
          $adjusted_items[] = $other_board_column_value;
        }
        $other_board_column->set($column_children_ref_field, $adjusted_items);
        $other_board_column->save();
      }
    }
  }


  /**
   * Process Task.
   */
  protected function processTask($item_data): ?EntityInterface {
    $config = $this->getConfig();
    $item_type = $item_data['entity_type'];
    $item_children_type = $this->pmConfig->getChildEntityType($item_type, '');
    $item_children_ref_type = $this->pmConfig->getChildReferenceFieldName($item_type, '');

    // @TODO Default bundle should come from upstream.
    $item_default_bundle = $config['defaults']['create']['bundle'][$item_type];
    $storage = $this
      ->entityTypeManager
      ->getStorage($item_type);
    $external_id = $item_data['id'];
    $drupal_id = $this->getDrupalId($external_id);
    /** @var \Drupal\pm\PmContentEntityBaseInterface $item */
    if ($drupal_id) {
      $item = $storage->load($drupal_id);
      if (empty($item)) {
        // @TODO: Handle deletion of item.
        return NULL;
      }
      // Bail out early in case of conflict.
      if (!empty($item_data['last_updated']) && $item->getChangedTime() != $item_data['last_updated']) {
        return $item;
      }
      $current_data = $this->getTaskData($item);
      // Bail out early if things are same.
      if (!$this->checkIsDirty('boards.columns.tasks', $current_data, $item_data)) {
        return $item;
      }
    }
    else {
      $item = $storage->create(
      [
        'bundle' => $item_default_bundle,
        'pm_project' => $this->getProject()->id(),
      ]
      );
      $item->save();
      $drupal_id = $item->id();
      $this->setDrupalId($external_id, $drupal_id);
    }
    $item->set('label', $item_data['title']);
    $item->set('summary', $item_data['description']);
    $items = [];
    foreach ($item_data['subtasks'] as $sub_item_data) {
      $sub_item_data['entity_type'] = $item_children_type;
      if ($sub_item = $this->processSubtask($sub_item_data)) {
        $items[] = $sub_item;
      }
    }
    $item->set($item_children_ref_type, $items);
    $item->save();
    return $item;
  }

  /**
   * Process Sub Tasks.
   */
  protected function processSubtask($sub_item_data): ?EntityInterface {
    $config = $this->getConfig();
    $sub_item_type = $sub_item_data['entity_type'];
    $sub_item_default_bundle = $config['defaults']['create']['bundle'][$sub_item_type];
    $storage = $this
      ->entityTypeManager
      ->getStorage($sub_item_type);
    $external_id = $sub_item_data['id'];
    $drupal_id = $this->getDrupalId($external_id);
    /** @var \Drupal\migrate\Plugin\migrate\destination\Entity $subtask */
    if ($drupal_id) {
      $subtask = $storage->load($drupal_id);
      if (empty($subtask)) {
        // @TODO Handle task got deleted.
        return NULL;
      }
      // Bail out early in case of conflict.
      if (!empty($sub_item_data['last_updated']) && $subtask->getChangedTime() != $sub_item_data['last_updated']) {
        return $subtask;
      }
      // Bail out early if things are same.
      $current_data = $this->getSubtaskData($subtask);
      if (!$this->checkIsDirty('boards.columns.tasks.subtasks', $current_data, $sub_item_data)) {
        return $subtask;
      }
    }
    else {
      $subtask = $storage->create(
        [
          'bundle' => $sub_item_default_bundle,
          'pm_project' => $this->getProject()->id(),
        ]
      );
      $subtask->save();
      $drupal_id = $subtask->id();
      $this->setDrupalId($external_id, $drupal_id);
    }
    $subtask->set('label', $sub_item_data['title']);
    if (!empty($sub_item_data['isCompleted'])) {
      $subtask->setDone();
    }
    else {
      $subtask->setOpen();
    }
    $subtask->save();
    return $subtask;
  }

  /**
   * Given an external id, provides back Drupal ID.
   *
   * @param string $external_id
   *   The external id.
   *
   * @return string|null
   *   Drupal ID if present.
   */
  protected function getDrupalId($external_id): ?string {
    if (is_numeric($external_id)) {
      return $external_id;
    }
    return $this->storage->get($external_id, NULL);
  }

  /**
   * Stores a map of external id with corresponding Drupal ID.
   *
   * @param string $external_id
   *   The external identifier.
   * @param string $drupalId
   *   Drupal identifier.
   */
  protected function setDrupalId($external_id, $drupalId) {
    $this->storage->set($external_id, $drupalId);
  }

}
