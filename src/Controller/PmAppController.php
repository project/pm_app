<?php

declare(strict_types=1);

namespace Drupal\pm_app\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for PM App routes.
 */
final class PmAppController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build(): array {

    $build['content'] = [
      '#markup' => 'PLACEHOLDER',
    ];

    return $build;
  }

}
