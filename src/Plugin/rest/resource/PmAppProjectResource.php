<?php

declare(strict_types=1);

namespace Drupal\pm_app\Plugin\rest\resource;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableResponse;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\pm\PmEtag;
use Drupal\pm_app\Service\PmAppKanbanService;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Represents Project records as resources.
 *
 * @RestResource (
 *   id = "pm_app_project",
 *   label = @Translation("PM App Project"),
 *   uri_paths = {
 *     "canonical" = "/api/pm_app/project/{id}"
 *   }
 * )
 * @see \Drupal\rest\Plugin\rest\resource\EntityResource
 */
final class PmAppProjectResource extends ResourceBase {

  /**
   * The key-value storage.
   */
  private readonly PmAppKanbanService $storage;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    private readonly KeyValueFactoryInterface $keyValueFactory,
    private readonly PmAppKanbanService $kanbanService,
    private readonly PmEtag $pmEtag,
    private readonly Request $request,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->storage = $kanbanService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('keyvalue'),
      $container->get('pm_app.kanban'),
      $container->get('pm.etag'),
      $container->get('request_stack')->getCurrentRequest()

    );
  }

  /**
   * Responds to GET requests.
   */
  public function get($id): ResourceResponse {

    if (!$this->storage->has($id)) {
      throw new NotFoundHttpException();
    }
    // @todo Add proper cacheable dependency.
    $build = [
      '#cache' => [
        'max-age' => 0,
      ]
    ];

    $etag = (string) $this->pmEtag->getEtag('pm_project', $id);
    $request = $this->request;
    if ($request->headers->has('If-None-Match')) {
      $match = $request->headers->get('If-None-Match');
      if ($etag === $match) {
        $not_modified_response = new ResourceResponse(NULL, 304);
        $not_modified_response->addCacheableDependency($build);
        return $not_modified_response;
      }
    }

    $resource = $this->storage->get($id);
    $response = new ResourceResponse($resource);

    $response->addCacheableDependency($build);
    $new_etag = $this->pmEtag->getEtag('pm_project', $id);
    $response->headers->set('ETag', (string) $new_etag);
    $response->headers->set('X-Drupal-ETag', (string) $new_etag);
    return $response;
  }

  /**
   * Responds to PATCH requests.
   */
  public function patch($id, array $data): ModifiedResourceResponse {
    if (!$this->storage->has($id)) {
      throw new NotFoundHttpException();
    }
    $this->storage->set($id, $data);
    $this->logger->notice('The project record @id has been updated.', ['@id' => $id]);
    $resource = $this->storage->get($id);
    return new ModifiedResourceResponse($resource, 200);
  }

}
