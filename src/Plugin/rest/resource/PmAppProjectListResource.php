<?php

declare(strict_types=1);

namespace Drupal\pm_app\Plugin\rest\resource;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Represents PM App Project List records as resources.
 *
 * @RestResource (
 *   id = "pm_app_project_list",
 *   label = @Translation("PM App Project List"),
 *   uri_paths = {
 *     "canonical" = "/api/pm_app/project",
 *   }
 * )
 */
final class PmAppProjectListResource extends ResourceBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    private EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Responds to GET requests.
   */
  public function get(): ResourceResponse {
    $projects = $this
      ->entityTypeManager
      ->getStorage('pm_project')
      ->loadMultiple();
    $resource = [];
    foreach ($projects as $project) {
      $resource[] = [
        'id' => $project->id(),
        'title' => $project->label(),
      ];
    }
    $response = new ResourceResponse($resource);
    $build = [
      '#cache' => [
        'tags' => ['pm_project_list'],
        'max-age' => Cache::PERMANENT,
      ]
    ];
    $response->addCacheableDependency($build);
    return $response;
  }

}
