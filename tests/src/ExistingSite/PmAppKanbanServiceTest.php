<?php

namespace Drupal\Tests\pm_app\ExistingSite;

use weitzman\DrupalTestTraits\ExistingSiteBase;

/**
 * Test Pm App Kanban Adapter.
 */
class PmAppKanbanServiceTest extends ExistingSiteBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;


  /**
   * The Kanban Adapter service.
   *
   * @var \Drupal\pm_app\Service\PmAppKanbanService
   */
  protected $pmKanban;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Cause tests to fail if an error is sent to Drupal logs.
    $this->failOnLoggedErrors();
    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->pmKanban = $this->container->get('pm_app.kanban');
  }

  /**
   * Tests translation configuration of entity bundles.
   */
  public function testProjectA() {
    $data = $this->pmKanban->get(1);
    $this->assertIsArray($data);
  }

}
